// [SECTION] JSON Object
/*
	- JavaScript Object Notation
	- A common use of JSON is to read data from web server, and display the data in a web page.
SYNTAX:
{
	"propertyA": "valueA",
	"propertyB": "valueB",
}
*/

// JSON Objects
/* {
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
	} */

// [SECTION] JSON Array

/*"cities": [
	{"city": "Quezon City","province": "Metro Manila","country": "Philippines"},
	{"city": "Pasig City","province": "Metro Manila","country": "Philippines"},
	{"city": "Navotas City","province": "Metro Manila","country": "Philippines"},
]
*/

// [SECTION] JSON Methods
/*
	JSON.stringify - converts JavaScript object/array into string.

	JSON.parse - converts JSON format to JavaScript object
*/

let batchesArr = [
	{batchName: 'Batch 177'},
	{batchName: 'Batch 178'},
	{batchName: 'Batch 179'},
];

console.log(batchesArr);


// Stringify
let batchArrCont = JSON.stringify(batchesArr)
console.log("Result from stringify method:");
console.log(batchArrCont);


let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines",
	}
})

console.log(data)

// JSON.parse
console.log("Result from parse method:");

let dataParse = JSON.parse(data)
console.log(dataParse);
console.log(dataParse.name);

console.log(JSON.parse(batchArrCont))



